FROM python:3
WORKDIR /usr/steve/src/
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . .
ENTRYPOINT ["python", "./src/app.py"]
