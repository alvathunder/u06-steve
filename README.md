# u06-steve

## Namn
Steve - The bot 

## Beskrivning
Steve är en personlig, interaktiv bot som fungerar med både röst- och textstyrning. Steve är baserad på en algoritm som känner igen ord, kalkylerar och matchar sedan användarinputen med en utav de förprogrammerade svaren som Steve erhåller. 
Steve genererar inte bara respons i form av förprogrammerade svar utan kan även webscrape för att exempelvis söka upp en låt på youtube. 

## Kommando
För att exekvera Steve:
```bash
python3 app.py
```

## Funktionalitet
Vid exekvering letar Steve efter en befinlig jsonfil, om denna finns så förstår Steve att programmet har exekverats tidigare hos denna användare. Om filen inte existerar kommer Steve att introducera sig, skapa jsonfilen, be om användarens namn och sedan lagra detta. 

Användaren kan sedan välja huruvida hen vill kommunicera med Steve via text eller tal. Detta val sker varje gång användaren exekverar Steve. 

### Textbaserad version
Steve tar inledningsvis emot en användarinput och kommer då att undersöka sannolikheten för att inputen är en korrekt mening. Steve kommer att dissikera meningen från inputen till enskilda ord och därefter se hur många av orden han känner igen. Varje ord som Steve känner igen och kan matcha till ett utav de förprogrammerade svaren, så kommer det ske en procentuell ökning som redogör för hur hög sannolikheten är att just den responsen är korrekt för inputen i fråga. Den förprogrammerade responsen som fick högst procentuell matchning kommer att bli den respons som presenteras till användaren. Se följande exempel:

```python
You: hey 
Steve: Hello!
You: what do you like to eat?
Steve: I frickin love cookies! I eat them one byte at a time.
```
I detta exempel så var den första användarinputen 'hey', vilket är en av de ord som Steve är programmerad att känna igen och generera svaret 'Hello!' åt. Samma respons hade givits om användaren hade skrivit något av följande: 'hello', 'hi', 'hey', 'sup', 'heyo'.

Därefter frågar användaren vad Steve tycker om att äta, här känner Steve igen orden 'what', 'you', 'eat'. Då alla dessa ord omfattades av inputen, så är responsen 'I frickin love cookies! I eat them one byte at a time.' det mest troliga. I detta fall så har denna respons två ord(mandatory_words) vars existens är ett krav för att responsen ska kunna genereras, vilket var orden 'you', 'eat'.

### Speech Recognition-baserad version
Vid exekvering av denna app kommer programmet skapa en koppling till användarens default mikrofon. Efter detta kalibreras bakgrundsljudet och sedan ge en output i form av "Listening..." för att indikerad på att användaren kan börja tala.
Även här kommer Steve att leta efter särskilda ord. Se följande exempel:

```python
Steve: Hello, how can I help you?
Listening...
You: hey steve, play thunderstruck by ac/dc 
Steve: Playing thunderstruck by ac/dc
#här öppnar Steve youtube på användarens webbläsare och spelar upp låten.
```

När Steve uppfattade ordet 'play' så förstod Steve att användaren ville spela upp en låt och använde sedan inputen 'thunderstruck by ac/dc' för att generera rätt låt. 

## Docker 
För att bygga en image:
```bash
docker build -t steve .
``` 
För att starta container:
```bash
docker run -it --rm steve
``` 

## Todo Sprint 2
Nuvarande backlog är följande:
- Skapa en meny där användaren kan välja mellan speech eller text-Steve.
- Skapa en def där Steve kan generera fram ett skämt.
- Skapa en def där Steve kan presentera hans humör med hjälp av ascii-art.
- Se till att Steve kan hämta användarens namn från jsonfil och printa namnet vid förslagvis användarens efterfrågan, eller vid varje greeting. 
- Slutföra tester (hittils utförts med hjälp av pytest)
 
Glöm inte att addera ändringar, eventuella funktioner eller dylikt till denna README.


