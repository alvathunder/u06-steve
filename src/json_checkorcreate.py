import json


def greeting():

    try:  # try to open file
        with open('file.json', "r") as f:
            # check = json.load(f)
            print('Hi name, so nice to see your again!')
    except FileNotFoundError:
        collect_userinfo()

def collect_userinfo():  # collecting users name

    check = []  # initiate list
    with open('file.json', "w") as f:  # create file
        json.dump(check, f, indent=4)

    with open("file.json", "r") as f:  # open file
        temp = json.load(f)

    data = {}  # initiate dict

    print("\nHi, my name is Steve! So nice to meet you! ")
    name = input("What's your name? ")
    data["name"] = name  # place value in dict

    temp.append(data)  # append new data
    with open("file.json", "w") as f:  # dump new data to file
        json.dump(temp, f, indent=4)
    print('Thank you so much!')

greeting()